package vizner;

public class menadzerPoruka {

    public String siftuj(String poruka, String kljuc, String stdAlfabet) {
        char[] kljucniAlfabet = null;
        char[] listaKarakteraPoruke = poruka.toCharArray();
        String sifrat = "";
        kljucniAlfabet = napraviKaraktere(kljuc, listaKarakteraPoruke);
        for (int i = 0; i < poruka.length(); i++) {
            if (Character.isLetter(poruka.charAt(i))) {
                System.out.println();
                int p = stdAlfabet.indexOf(poruka.charAt(i));
                int k = stdAlfabet.indexOf(kljucniAlfabet[i]);
                int s = (p + k) % stdAlfabet.length();
                sifrat = sifrat + stdAlfabet.charAt(s);
            } else {
                sifrat = sifrat + poruka.charAt(i);
            }
        }
        return sifrat;
    }

    private static char[] napraviKaraktere(String sifra, char[] poruka) {
        int n = 0;
        char[] sifrat = new char[poruka.length];
        for (int i = 0; i < poruka.length; i++) {
            if (Character.isLetter(poruka[i])) {
                sifrat[i] = sifra.charAt(n);
                n++;
            } else {
                sifrat[i] = poruka[i];
            }
            if (sifra.length() == n) {
                n = 0;
            }
        }
        return sifrat;
    }

    public String desifruj(String sifrat, String kljuc, String stdAlfabet) {
        char[] kljucniAlfabet = null;
        char[] listaKarakteraPoruke = sifrat.toCharArray();
        String porukaDes = "";
        kljucniAlfabet = napraviKaraktere(kljuc, listaKarakteraPoruke);
        for (int i = 0; i < sifrat.length(); i++) {
            if (Character.isLetter(sifrat.charAt(i))) {
                System.out.println();
                int p = stdAlfabet.indexOf(sifrat.charAt(i));
                int k = stdAlfabet.indexOf(kljucniAlfabet[i]);
                int s = (p - k + stdAlfabet.length()) % stdAlfabet.length();
                porukaDes = porukaDes + stdAlfabet.charAt(s);
            } else {
                porukaDes = porukaDes + sifrat.charAt(i);
            }
        }
        return porukaDes;
    }
}
